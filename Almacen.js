
function Almacen(){
    this.ordenadores = [];
}
Almacen.prototype.alta = function (ordenador){
    if ((ordenador) != null && (ordenador).constructor.name == "Ordenador")
        this.ordenadores.push((ordenador));
};

Almacen.prototype.baja = function (n){
    if(!isNaN(n) && n < this.ordenadores.length) {
        return this.ordenadores.splice(n, 1);
    }else{
        alert("Fuera de rango");
    }
};

Almacen.prototype.precioMinimo = function () {
    if(this.ordenadores.length == 0){alert("Array vacio");return;}
    var x;
    var ordenador = this.ordenadores[0];
    for (x in this.ordenadores){
        if(this.ordenadores[x].precio < ordenador.precio){
            ordenador = this.ordenadores[x];
        }
    }
    return ordenador.mostrarDatos();
};
Almacen.prototype.precioMaximo = function () {
    if(this.ordenadores.length == 0){alert("Array vacio");return;}
    var x;
    var ordenador = this.ordenadores[0];
    for (x in this.ordenadores){
        if(this.ordenadores[x].precio > ordenador.precio){
            ordenador = this.ordenadores[x];
        }
    }
    return ordenador.mostrarDatos();
};
Almacen.prototype.mayor_A = function (precio) {
    if(this.ordenadores.length == 0){alert("Array vacio");return;}
    if(isNaN(precio)){alert("Inserte un numero");return;}
    var x;
    var contador = 0;
    for (x in this.ordenadores){
        if(this.ordenadores[x].precio > precio){
            contador++;
        }
    }
    if(contador == 1){
        return contador + " ordenador supera el precio de " + precio;
    }else if(contador == 0){
        return "Ningun ordenador supera " + precio;
    }
    return contador + " ordenadores superan el precio de " + precio;
};
Almacen.prototype.menor_A = function (precio) {
    if(this.ordenadores.length == 0){alert("Array vacio");return;}
    if(isNaN(precio)){alert("Inserte un numero");return;}
    var x;
    var contador = 0;
    for (x in this.ordenadores){
        if(this.ordenadores[x].precio < precio){
            contador++;
        }
    }
    if(contador == 1){
        return contador + " ordenador esta por debajo de " + precio;
    }else if(contador == 0){
        return "Ningun ordenador esta por debajo de " + precio;
    }
    return contador + " ordenadores estan por debajo de " + precio;
};